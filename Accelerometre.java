package dreschmannjacquot.ensea.fr;

import android.app.Service;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class Accelerometre extends AppCompatActivity {


    float X; //l’accélération sur l’axe X
    float Y;   //l’accélération sur l’axe Y
    JeuActivity mActivity;// l’activité Jeu
    private SensorManager mManager = null;
    private Sensor mAccelerometre = null;




    private static final String TAG = Accelerometre.class.getSimpleName(); // activity name for debug log


    SensorEventListener mSensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent pEvent) {
            X =  pEvent.values[0];
            Y =  pEvent.values[1];
            Log.d(JeuActivity.TAG, "new sensor values: X=" + X + " Y=" + Y);
        }
        @Override
        public void onAccuracyChanged(Sensor pSensor, int pAccuracy) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometre);
        if (savedInstanceState != null) {
            Log.d(TAG, "onCreate() Restoring previous state");
            // restore state
        } else {
            Log.d(TAG, "onCreate() No saved state available");
            // initialize app
        }
        mManager = (SensorManager) mActivity.getBaseContext().getSystemService(Service.SENSOR_SERVICE);
        mAccelerometre = mManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        mManager.registerListener(mSensorEventListener, mAccelerometre, SensorManager.SENSOR_DELAY_GAME);

    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
        mManager.unregisterListener(mSensorEventListener, mAccelerometre);
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart()");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }
}
