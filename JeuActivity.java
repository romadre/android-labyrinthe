package dreschmannjacquot.ensea.fr;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log; // Log classes for debug
import android.view.Display;
import android.widget.Toast;

import java.util.List;

public class JeuActivity extends AppCompatActivity{
        protected static final String TAG = JeuActivity.class.getSimpleName(); // activity name for debug log
        public String nomJoueur;

    // Identifiant de la boîte de dialogue de victoire
    public static final int VICTORY_DIALOG = 0;
    // Identifiant de la boîte de dialogue de défaite
    public static final int DEFEAT_DIALOG = 1;

    // Le moteur graphique du jeu
    private Moteur_Graphique mView = null;
    // Le moteur physique du jeu
    private Moteur_Physique mEngine = null;

         @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState); // Always call the superclass method first
            setContentView(R.layout.activity_jeu);
            if (savedInstanceState != null) {
                Log.d(TAG, "onCreate() Restoring previous state");
                // restore state
            } else {
                Log.d(TAG, "onCreate() No saved state available");
                // initialize app
            }

             //récupération du nom du joueur
             Intent intent = getIntent();
             nomJoueur = intent.getStringExtra(MainActivity.NOM);
             Log.d(TAG, "Player name = " + nomJoueur);
             Toast.makeText(this, "Player name = " + nomJoueur, Toast.LENGTH_LONG).show();

             Display display = getWindowManager().getDefaultDisplay();
             Point size = new Point();
             display.getSize(size);
             int width = size.x;
             int height = size.y;
             Log.d(TAG, "dim = " + width + " * " + height);



             mView = new Moteur_Graphique(this);
             setContentView(mView);

             mEngine = new Moteur_Physique(this);

             Bille b = new Bille();
             mView.setBille(b);
             mEngine.setBille(b);

             List<Bloc> mList = mEngine.buildLabyrinthe();
             mView.setBlocks(mList);
        }
        @Override
        protected void onStart() {
            super.onStart();
            Log.d(TAG, "onStart()");
        }
        @Override
        protected void onResume() {
            super.onResume();
            Log.d(TAG, "onResume()");
            mEngine.resume();
        }
        @Override
        protected void onPause() {
            super.onPause();
            Log.d(TAG, "onPause()");
        }
        @Override
        protected void onStop() {
            super.onStop();
            Log.d(TAG, "onStop()");
            mEngine.stop();
        }
        @Override
        protected void onRestart() {
            super.onRestart();
            Log.d(TAG, "onRestart()");
        }
        @Override
        protected void onDestroy() {
            super.onDestroy();
            Log.d(TAG, "onDestroy()");
        }


    @Override
    public Dialog onCreateDialog (int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch(id) {
            case VICTORY_DIALOG:
                builder.setCancelable(false)
                        .setMessage("Bravo, vous avez gagné !")
                        .setTitle("Félicitations! Vous êtes le meilleur !")
                        .setNeutralButton("Recommencer", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // L'utilisateur peut recommencer s'il le veut
                                mEngine.reset();
                                mEngine.resume();
                            }
                        });
                break;

            case DEFEAT_DIALOG:
                builder.setCancelable(false)
                        .setMessage("Dommage, vous êtes tombés dans le trou !")
                        .setTitle("Bah bravo Nils !")
                        .setNeutralButton("Recommencer", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mEngine.reset();
                                mEngine.resume();
                            }
                        });
        }
        return builder.create();
    }
    @Override
    public void onPrepareDialog (int id, Dialog box) {
        // A chaque fois qu'une boîte de dialogue est lancée, on arrête le moteur physique
        mEngine.stop();
    }
}

