package dreschmannjacquot.ensea.fr;
/*
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}*/


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log; // Log classes for debug
import android.widget.Button;
import android.widget.EditText;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName(); // activity name for debug log
    public final static String NOM = "fr.ensea.dreschmannjacquot.application1.intent.NOM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // Always call the superclass method first
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            Log.d(TAG, "onCreate() Restoring previous state");
            // restore state
        } else {
            Log.d(TAG, "onCreate() No saved state available");
            // initialize app
        }

        Button bJouer = (Button) findViewById(R.id.buttonJouer);
        bJouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, JeuActivity.class);
                EditText playerName = (EditText) findViewById(R.id.editTextPlayerName);
                Log.d(TAG, "Player name = " + playerName.getText());
                intent.putExtra(NOM, "" + playerName.getText().toString()); /* On ajoute toString() pour
            convertir le android.text.SpannableString en string */
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");

    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart()");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }




}