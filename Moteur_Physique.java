package dreschmannjacquot.ensea.fr;

        import java.util.ArrayList;
        import java.util.List;
        import dreschmannjacquot.ensea.fr.Bloc.Type;
        import android.app.Service;
        import android.graphics.RectF;
        import android.hardware.Sensor;
        import android.hardware.SensorEvent;
        import android.hardware.SensorEventListener;
        import android.hardware.SensorManager;

public class Moteur_Physique {
    private Bille mBille = null;
    public Bille getBille() {
        return mBille;
    }

    public void setBille(Bille pBille) {
        this.mBille = pBille;
    }

    // Le labyrinthe
    private List<Bloc> mBlocks = null;

    private JeuActivity mActivity = null;

    private SensorManager mManager = null;
    private Sensor mAccelerometre = null;

    SensorEventListener mSensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent pEvent) {
            float y = pEvent.values[0];
            float x = pEvent.values[1];

            if(mBille != null) {
                // On met à jour les coordonnées de la boule
                RectF hitBox = mBille.putXAndY(x, y);

                // Pour tous les blocs du labyrinthe
                for(Bloc block : mBlocks) {
                    // On crée un nouveau rectangle pour ne pas modifier celui du bloc
                    RectF inter = new RectF(block.getRectangle());
                    if(inter.intersect(hitBox)) {
                        // On agit différement en fonction du type de bloc
                        switch(block.getType()) {
                            case TROU:
                                mActivity.showDialog(JeuActivity.DEFEAT_DIALOG);
                                break;

                            case DEPART:
                                break;

                            case ARRIVEE:
                                mActivity.showDialog(JeuActivity.VICTORY_DIALOG);
                                break;
                            case MURV:
                                mBille.changeYSpeed();
                                break;
                            case MURH:
                                mBille.changeXSpeed();
                                break;
                        }
                        break;
                    }
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor pSensor, int pAccuracy) {

        }
    };

    public Moteur_Physique(JeuActivity pView) {
        mActivity = pView;
        mManager = (SensorManager) mActivity.getBaseContext().getSystemService(Service.SENSOR_SERVICE);
        mAccelerometre = mManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    // Remet à zéro l'emplacement de la boule
    public void reset() {
        mBille.reset();
    }

    // Arrête le capteur
    public void stop() {
        mManager.unregisterListener(mSensorEventListener, mAccelerometre);
    }

    // Redémarre le capteur
    public void resume() {
        mManager.registerListener(mSensorEventListener, mAccelerometre, SensorManager.SENSOR_DELAY_GAME);
    }

    // Construit le labyrinthe
    public List<Bloc> buildLabyrinthe() {

        mBlocks = new ArrayList<Bloc>();/*
        mBlocks.add(new Bloc(Type.MURV, 0, 0));
        mBlocks.add(new Bloc(Type.MURH, 0, 1));
        mBlocks.add(new Bloc(Type.MURH, 0, 2));
        mBlocks.add(new Bloc(Type.MURH, 0, 3));
        mBlocks.add(new Bloc(Type.MURH, 0, 4));
        mBlocks.add(new Bloc(Type.MURH, 0, 5));
        mBlocks.add(new Bloc(Type.MURH, 0, 6));
        mBlocks.add(new Bloc(Type.MURH, 0, 7));
        mBlocks.add(new Bloc(Type.MURH, 0, 8));
        mBlocks.add(new Bloc(Type.MURH, 0, 9));
        mBlocks.add(new Bloc(Type.MURH, 0, 10));
        mBlocks.add(new Bloc(Type.MURH, 0, 11));
        mBlocks.add(new Bloc(Type.MURH, 0, 12));
        mBlocks.add(new Bloc(Type.MURV, 0, 13));

        mBlocks.add(new Bloc(Type.MURV, 1, 0));
        mBlocks.add(new Bloc(Type.MURV, 1, 13));

        mBlocks.add(new Bloc(Type.MURV, 2, 0));
        mBlocks.add(new Bloc(Type.MURV, 2, 13));

        mBlocks.add(new Bloc(Type.MURV, 3, 0));
        mBlocks.add(new Bloc(Type.MURV, 3, 13));

        mBlocks.add(new Bloc(Type.MURV, 4, 0));
        mBlocks.add(new Bloc(Type.MURH, 4, 1));
        mBlocks.add(new Bloc(Type.MURH, 4, 2));
        mBlocks.add(new Bloc(Type.MURH, 4, 3));
        mBlocks.add(new Bloc(Type.MURH, 4, 4));
        mBlocks.add(new Bloc(Type.MURH, 4, 5));
        mBlocks.add(new Bloc(Type.MURH, 4, 6));
        mBlocks.add(new Bloc(Type.MURH, 4, 7));
        mBlocks.add(new Bloc(Type.MURH, 4, 8));
        mBlocks.add(new Bloc(Type.MURH, 4, 9));
        mBlocks.add(new Bloc(Type.MURH, 4, 10));
        mBlocks.add(new Bloc(Type.MURV, 4, 13));

        mBlocks.add(new Bloc(Type.MURV, 5, 0));
        mBlocks.add(new Bloc(Type.MURV, 5, 13));

        mBlocks.add(new Bloc(Type.MURV, 6, 0));
        mBlocks.add(new Bloc(Type.MURV, 6, 13));

        mBlocks.add(new Bloc(Type.MURV, 7, 0));
        mBlocks.add(new Bloc(Type.MURH, 7, 1));
        mBlocks.add(new Bloc(Type.MURH, 7, 2));
        mBlocks.add(new Bloc(Type.MURH, 7, 5));
        mBlocks.add(new Bloc(Type.MURH, 7, 6));
        mBlocks.add(new Bloc(Type.MURH, 7, 9));
        mBlocks.add(new Bloc(Type.MURH, 7, 10));
        mBlocks.add(new Bloc(Type.MURH, 7, 11));
        mBlocks.add(new Bloc(Type.MURH, 7, 12));
        mBlocks.add(new Bloc(Type.MURV, 7, 13));

        mBlocks.add(new Bloc(Type.MURV, 8, 0));
        mBlocks.add(new Bloc(Type.MURH, 8, 5));
        mBlocks.add(new Bloc(Type.MURH, 8, 9));
        mBlocks.add(new Bloc(Type.MURV, 8, 13));

        mBlocks.add(new Bloc(Type.MURV, 9, 0));
        mBlocks.add(new Bloc(Type.MURH, 9, 5));
        mBlocks.add(new Bloc(Type.MURH, 9, 9));
        mBlocks.add(new Bloc(Type.MURV, 9, 13));

        mBlocks.add(new Bloc(Type.MURV, 10, 0));
        mBlocks.add(new Bloc(Type.MURH, 10, 5));
        mBlocks.add(new Bloc(Type.MURH, 10, 9));
        mBlocks.add(new Bloc(Type.MURV, 10, 13));

        mBlocks.add(new Bloc(Type.MURV, 11, 0));
        mBlocks.add(new Bloc(Type.MURH, 11, 5));
        mBlocks.add(new Bloc(Type.MURH, 11, 9));
        mBlocks.add(new Bloc(Type.MURV, 11, 13));

        mBlocks.add(new Bloc(Type.MURV, 12, 0));
        mBlocks.add(new Bloc(Type.MURH, 12, 1));
        mBlocks.add(new Bloc(Type.MURH, 12, 2));
        mBlocks.add(new Bloc(Type.MURH, 12, 3));
        mBlocks.add(new Bloc(Type.MURH, 12, 4));
        mBlocks.add(new Bloc(Type.MURH, 12, 5));
        mBlocks.add(new Bloc(Type.MURH, 12, 9));
        mBlocks.add(new Bloc(Type.MURH, 12, 8));
        mBlocks.add(new Bloc(Type.MURV, 12, 13));

        mBlocks.add(new Bloc(Type.MURV, 13, 0));
        mBlocks.add(new Bloc(Type.MURH, 13, 8));
        mBlocks.add(new Bloc(Type.MURV, 13, 13));

        mBlocks.add(new Bloc(Type.MURV, 14, 0));
        mBlocks.add(new Bloc(Type.MURH, 14, 8));
        mBlocks.add(new Bloc(Type.MURV, 14, 13));

        mBlocks.add(new Bloc(Type.MURV, 15, 0));
        mBlocks.add(new Bloc(Type.MURH, 15, 8));
        mBlocks.add(new Bloc(Type.MURV, 15, 13));

        mBlocks.add(new Bloc(Type.MURV, 16, 0));
        mBlocks.add(new Bloc(Type.MURH, 16, 4));
        mBlocks.add(new Bloc(Type.MURH, 16, 5));
        mBlocks.add(new Bloc(Type.MURH, 16, 6));
        mBlocks.add(new Bloc(Type.MURH, 16, 7));
        mBlocks.add(new Bloc(Type.MURH, 16, 8));
        mBlocks.add(new Bloc(Type.MURH, 16, 9));
        mBlocks.add(new Bloc(Type.MURV, 16, 13));

        mBlocks.add(new Bloc(Type.MURV, 17, 0));
        mBlocks.add(new Bloc(Type.MURV, 17, 13));

        mBlocks.add(new Bloc(Type.MURV, 18, 0));
        mBlocks.add(new Bloc(Type.MURV, 18, 13));

        mBlocks.add(new Bloc(Type.MURV, 19, 0));
        mBlocks.add(new Bloc(Type.MURH, 19, 1));
        mBlocks.add(new Bloc(Type.MURH, 19, 2));
        mBlocks.add(new Bloc(Type.MURH, 19, 3));
        mBlocks.add(new Bloc(Type.MURH, 19, 4));
        mBlocks.add(new Bloc(Type.MURH, 19, 5));
        mBlocks.add(new Bloc(Type.MURH, 19, 6));
        mBlocks.add(new Bloc(Type.MURH, 19, 7));
        mBlocks.add(new Bloc(Type.MURH, 19, 8));
        mBlocks.add(new Bloc(Type.MURH, 19, 9));
        mBlocks.add(new Bloc(Type.MURH, 19, 10));
        mBlocks.add(new Bloc(Type.MURH, 19, 11));
        mBlocks.add(new Bloc(Type.MURH, 19, 12));
        mBlocks.add(new Bloc(Type.MURV, 19, 13));

*/
        Bloc b = new Bloc(Type.DEPART, 2, 2);
        mBille.setInitialRectangle(new RectF(b.getRectangle()));
        mBlocks.add(b);

        mBlocks.add(new Bloc(Type.ARRIVEE, 8, 11));

        return mBlocks;
    }

}
